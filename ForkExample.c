#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>

int input(char *s,int length);
int status;

char *addCharToString(char* str, char c){
	
    size_t len = strlen(str);
    char *str2 = malloc(len + 1 + 1 ); /* one for extra char, one for trailing zero */
    strcpy(str2, str);
    str2[len] = c;
    str2[len + 1] = '\0';
	return str2;
	
}

char *getInput(char *buffer, size_t bufsize){
	//&buffer - adress if first character where position where input string will be stored
	//&bufsize - adress of the size of buffer
	int i = getline(&buffer,&bufsize,stdin);
	if (i == -1){
		perror("Exception. Can't load command content.\n");
		exit(-1);
	}
	buffer[strlen(buffer)-1] = 0;
	return buffer;
}

char *getFunction(char *Command){
	char *ret = "";
	int i;
	for (i=0; Command[i]; i++){
		if (Command[i] != ' '){
			ret = addCharToString(ret, Command[i]);
		}else{
			return ret;
		}
		if (strlen(Command) - 1 == i)
			return ret; 
	}
  	perror("Exception. Can't load command name.\n");
	exit(2);
}

char** getArguments(char *Command){
    
	//count how many argument user provided (number of ' ' signs)
	int i, count, len;
    len = strlen(Command);
	for (i=0, count=0; i < len; i++)
        count += (Command[i] == ' ');
	
	char** args = malloc((count+2) * sizeof(char*));
	
	if (args == NULL){
		perror("Unable to alloce space for arguments array");
		exit(1);
	}
    	
    int argNumber = 0;
	char *arg = "";
    i=0;
    
    for (i=0; i < len; i++){
        if (Command[i] != ' '){
            arg = addCharToString(arg, Command[i]);
        }else{
            args[argNumber] = arg;
            arg = "";
            argNumber++;
        }
        if (strlen(Command) - 1 == i){
            args[argNumber] = arg;
        }
	}
	
	args[count + 1] = NULL;
	
	return args;
}

int main(){
	//place to store string input
    char *buffer;
	//size_t - special type of int required by getline function
    size_t bufsize = 32;
	
	//assign 32 bites of memory to buffer location
    buffer = (char *)malloc(bufsize * sizeof(char));
	//check if if was succesful (was free space to locate)
    if( buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }
	
	while (1){
		
		printf("Write command: ");
		char *p = getInput(buffer, bufsize);
        
		if (!strcmp(p,"quit")){
			printf("Exiting application\n");
			exit(0);
		}
		
		pid_t pid = fork();
		if (pid == 0){
            
			char* header = getFunction(p);
            char** arguments = getArguments(p);
            
			execvp(header, arguments);
        	perror("Exception. Can't proceed desired command.\n");
			exit(1);
		}
    	waitpid(pid, &status, 0);
		if (status != 0){
			perror("Exception. Command didn't succeed to proceed.\n");
		}
		
	}
    return(0);
}

	